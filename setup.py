#!/usr/bin/env python3
# This is only to be able to python3 -m pip install --editable ./
import setuptools
if __name__ == "__main__":
    setuptools.setup()
