Decoratio Changelog
=========

Decoratio 1.0 - 2022-10-03
-----------------------------

Initial release.

1.0.1: Fixed package dependencies.

1.0.2: Updated documenation.

1.0.3: Fixed deprecated use of np.float

1.0.4: Fixed the documentation (GATK-Markduplicates section).

1.0.5: Now also prints the Lander-Waterman (noiseless) complexity.
